package com.runescape.shared.network;

/**
 * Provides an interface for shared packet handlers
 * 
 * @author Hikilaka
 *
 */
public interface ClientMessageListener {

	/**
	 * @return The packet id's this PacketHandler will handle
	 */
	public int[] getAssociatedIds();

	/**
	 * Invoked when a packet is received
	 * 
	 * @param session The {@link Session} who sent the packet
	 * @param packet The {@link Packet} that was sent
	 */
	public void onMessage(Packet packet) throws PacketReadException;

}
