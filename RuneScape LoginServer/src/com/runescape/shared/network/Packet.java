package com.runescape.shared.network;

/**
 * A packet object that the server will send or receive
 * 
 * @author Hikilaka
 *
 */
public final class Packet {

	private final Session session;

	private final int id;

	private final byte[] data;

	private final int length;

	private final boolean bare;

	private int caret;

	public Packet(Session session, int id, byte[] data) {
		this(session, id, data, false);
	}

	public Packet(Session session, int id, byte[] data, boolean bare) {
		this.session = session;
		this.id = id;
		this.data = data;
		this.length = data.length;
		this.bare = bare;
	}

	public Session getSession() {
		return session;
	}

	public int getId() {
		return id;
	}

	public byte[] getData() {
		return data;
	}

	public int getLength() {
		return length;
	}

	public byte[] getRemainingData() {
		byte[] data = new byte[length - caret];
		for (int i = 0; i < data.length; i++) {
			data[i] = data[i + caret];
		}
		caret += data.length;
		return data;
	}

	public boolean isBare() {
		return bare;
	}

	public int readByte() throws PacketReadException {
		return data[caret++];
	}

	public boolean readBoolean() throws PacketReadException {
		return data[caret++] == 1;
	}

	public short readShort() throws PacketReadException {
		return (short) ((short) ((data[caret++] & 0xff) << 8) | (short) (data[caret++] & 0xff));
	}

	public int readInt() throws PacketReadException {
		return ((data[caret++] & 0xff) << 24)
				| ((data[caret++] & 0xff) << 16)
				| ((data[caret++] & 0xff) << 8) | (data[caret++] & 0xff);
	}

	public long readLong() throws PacketReadException {
		return (long) ((long) (data[caret++] & 0xff) << 56)
				| ((long) (data[caret++] & 0xff) << 48)
				| ((long) (data[caret++] & 0xff) << 40)
				| ((long) (data[caret++] & 0xff) << 32)
				| ((long) (data[caret++] & 0xff) << 24)
				| ((long) (data[caret++] & 0xff) << 16)
				| ((long) (data[caret++] & 0xff) << 8)
				| ((long) (data[caret++] & 0xff));
	}

	public byte[] readBytes(int length) throws PacketReadException {
		byte[] data = new byte[length];
		for (int i = 0; i < length; i++) {
			data[i] = this.data[i + caret];
		}
		caret += length;
		return data;
	}

	public String readString() throws PacketReadException {
		return readString(length - caret);
	}

	public String readString(int length) throws PacketReadException {
		String rv = new String(data, caret, length);
		caret += length;
		return rv;
	}

	public int remaining() {
		return data.length - caret;
	}

	public Packet skip(int x) {
		caret += x;
		return this;
	}

}