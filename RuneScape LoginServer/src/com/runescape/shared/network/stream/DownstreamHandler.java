package com.runescape.shared.network.stream;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelDownstreamHandler;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;

import com.runescape.shared.network.Packet;

/**
 * Provides a network packet encoder
 * 
 * @author Hikilaka
 *
 */
public final class DownstreamHandler implements ChannelDownstreamHandler {

	@Override
	public void handleDownstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
		if (!(e instanceof MessageEvent)) {
			ctx.sendUpstream(e);
			return;
		}
		MessageEvent event = MessageEvent.class.cast(e);
		Packet packet = Packet.class.cast(event.getMessage());
		ChannelBuffer buffer = null;
		int length = packet.getLength();

		if (packet.isBare()) {
			// If the packet is bare, we just send the
			// raw data, no formatting it, hoorah!
			buffer = ChannelBuffers.buffer(length);
			buffer.writeBytes(packet.getData());
		} else {
			// In this case we need the length (possibly two bytes),
			// the packet id, and it's data
			buffer = ChannelBuffers.buffer(length + (length >= 160 ? 3 : 2));

			// The client reads the packet id as part of it's length
			length += 1;
			
			// If the packet length is greater
			// than or equal to 160, we split the length
			// into two bytes, otherwise, we include the LAST
			// byte of the packet's payload
			if (length >= 160) {
				buffer.writeByte(160 + length / 256);
				buffer.writeByte(length & 0xff);
			} else {
				buffer.writeByte(length);
				buffer.writeByte(packet.getData()[length - 2]);
			}

			// Don't forget the packet id
			buffer.writeByte(packet.getId());

			// Move the packet data to the buffer
			if (length >= 160) {
				buffer.writeBytes(packet.getData());
			} else {
				// Here we add the packets payload, except
				// the last byte, since it was included in the
				// header of the packet
				byte[] data = new byte[length - 2];
				System.arraycopy(packet.getData(), 0, data, 0, data.length);
				buffer.writeBytes(data);
			}
		}

		Channels.write(ctx, e.getFuture(), buffer, event.getRemoteAddress());
	}

}
