package com.runescape.shared.network.stream;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelUpstreamHandler;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;

import com.runescape.shared.network.Packet;
import com.runescape.shared.network.Session;

/**
 * Provides a network packet decoder
 * 
 * @author Hikilaka
 *
 */
public final class UpstreamHandler implements ChannelUpstreamHandler {

	@Override
	public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
		if (!(e instanceof MessageEvent)) {
			ctx.sendUpstream(e);
			return;
		}
		System.out.println("decoding!");
		MessageEvent event = MessageEvent.class.cast(e);
		ChannelBuffer buffer = ChannelBuffer.class.cast(event.getMessage());
		Session session = Session.class.cast(event.getChannel().getAttachment());

		if (buffer.readableBytes() < 1) {
			// The buffer contains invalid headers
			ctx.sendUpstream(e);
			return;
		}
		int length = buffer.readByte() & 0xff;

		// Does this packet header require us to read two bytes?
		if (length >= 160 && buffer.readable()) {
			length = (length - 160) * 256 + (buffer.readByte() & 0xff);
		}

		// Is the length greater than 0? Are we able to read the packet length?
		if (length > 0 && buffer.readableBytes() == length) {
			byte[] data = new byte[length];

			if (length >= 160) {
				buffer.readBytes(data, 0, length);
			} else {
				data[length - 1] = buffer.readByte();
				if (length > 1) {
					buffer.readBytes(data, 0, length - 2);
				}
			}
			int id = data[0] & 0xff;
			byte[] payload = new byte[length - 1]; // -1 due to reading the ID

			// Copy the packet data to a buffer
			System.arraycopy(data, 1, payload, 0, length - 1);
			Channels.fireMessageReceived(ctx, new Packet(session, id, payload), event.getRemoteAddress());
		}
	}

}