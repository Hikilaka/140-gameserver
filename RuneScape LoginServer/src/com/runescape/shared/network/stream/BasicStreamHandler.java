package com.runescape.shared.network.stream;

import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelState;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ChannelUpstreamHandler;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;

import com.runescape.shared.network.Packet;
import com.runescape.shared.network.Session;

/**
 * 
 * @author Hikilaka
 *
 */
public abstract class BasicStreamHandler implements ChannelUpstreamHandler {

	protected abstract void onConnected(Session session);

	protected abstract void onDisconnected(Session session);

	protected abstract void onMessage(Session session, Packet packet);

	protected abstract void onException(Throwable throwable);

	@Override
	public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent event) throws Exception {
		if (event instanceof MessageEvent) {
			// When a message has been fired from the UpstreamHandler
			Session session = Session.class.cast(ctx.getChannel().getAttachment());
			onMessage(session, Packet.class.cast(MessageEvent.class.cast(event).getMessage()));
		} else if (event instanceof ChannelStateEvent) {
			ChannelStateEvent stateEvent = ChannelStateEvent.class.cast(event);

			if (stateEvent.getState() == ChannelState.CONNECTED) {
				if (stateEvent.getValue() != null) {
					onConnected(new Session(ctx.getChannel()));
				} else {
					Session session = Session.class.cast(ctx.getChannel().getAttachment());
					onDisconnected(session);
				}
			} else {
				ctx.sendUpstream(event);
			}
		} else if (event instanceof ExceptionEvent) {
			onException(ExceptionEvent.class.cast(event).getCause());
		} else {
			ctx.sendUpstream(event);
		}
	}

}
