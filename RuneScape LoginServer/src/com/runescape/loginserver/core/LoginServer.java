package com.runescape.loginserver.core;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import com.runescape.loginserver.core.model.WorldHandler;
import com.runescape.loginserver.core.network.LoginPipelineFactory;
import com.runescape.loginserver.core.util.Settings;
import com.runescape.loginserver.plugins.PluginHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class LoginServer {

	private static final LoginServer INSTANCE = new LoginServer();

	public static LoginServer getLoginServer() {
		return INSTANCE;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Starting up " + Settings.NAME + " login server..");

		System.out.println("Loading plugins..");
		PluginHandler.getInstance().loadPlugins();

		INSTANCE.initiate();
		System.out.println(Settings.NAME + " login server is now online!");
	}

	private final LoginEngine loginEngine = new LoginEngine();

	private final WorldHandler worldHandler = new WorldHandler();

	private Channel channel;

	/**
	 * Initiates the server networking
	 */
	private void initiate() {
		ExecutorService workers = Executors.newFixedThreadPool(Settings.CORES * 2);
		ChannelFactory channelFactory = new NioServerSocketChannelFactory(workers, workers);
		ServerBootstrap bootstrap = new ServerBootstrap(channelFactory);

		bootstrap.setPipelineFactory(new LoginPipelineFactory());
		bootstrap.setOption("tcpNoDelay", true);
		bootstrap.setOption("keepAlive", false);

		channel = bootstrap.bind(new InetSocketAddress(Settings.PORT));

		if (channel.isOpen()) {
			loginEngine.setRunning(true);
			loginEngine.start();
		}
	}

	public LoginEngine getLoginEngine() {
		return loginEngine;
	}

	public WorldHandler getWorldHandler() {
		return worldHandler;
	}

}
