package com.runescape.loginserver.core;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import com.runescape.shared.network.ClientMessageListener;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketReadException;

public final class LoginEngine  extends Thread {

	public static final int TICK_RATE = 500;

	private static final LoginEngine INSTANCE = new LoginEngine();

	public static LoginEngine getGameEngine() {
		return INSTANCE;
	}

	/**
	 * Whether or not the game engine is running
	 */
	private volatile boolean running = false;

	/**
	 * A queue of received packets to be processed the next tick
	 */
	private final LinkedBlockingQueue<Packet> queuedPackets = new LinkedBlockingQueue<>();

	/**
	 * An array of ClientMessageListeners to handle incoming packets 
	 */
	private final ClientMessageListener[] clientMessageListeners = new ClientMessageListener[256];

	/**
	 * 
	 */
	@Override
	public void run() {
		long last = System.currentTimeMillis();

		while (running) {
			long start = System.currentTimeMillis();
			tick();

			long sleeptime = TICK_RATE - (start - last);
			if (sleeptime > 0) {
				try {
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				System.err.println("WARNING: " + (100 + (Math.abs(sleeptime) / (TICK_RATE / 100))) + "% server overload!");
			}
			last = System.currentTimeMillis();
		}
	}

	/**
	 * Processes incoming packets
	 */
	private void tick() {
		Iterator<Packet> it = queuedPackets.iterator();

		while (it.hasNext()) {
			Packet packet = it.next();
			ClientMessageListener handler = clientMessageListeners[packet.getId()];

			if (handler != null) {
				try {
					handler.onMessage(packet);
				} catch (PacketReadException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("WARNING: No packet handler for: " + packet.getId() + " " + Arrays.toString(packet.getData()));
			}

			// Remove the packet from the queue
			it.remove();
		}
	}

	/**
	 * Adds a packet to the login engine queue, to be 
	 * processed the next tick
	 * 
	 * @param packet The packet to queue
	 */
	public void queuePacket(Packet packet) {
		synchronized (queuedPackets) {
			queuedPackets.add(packet);
		}
	}

	/**
	 * Adds a {@link ClientMessageListener} to an array if not already present
	 * 
	 * @param listener The packet listener to register
	 */
	public void registerClientMessageListener(ClientMessageListener listener) {
		for (int id : listener.getAssociatedIds()) {
			if (clientMessageListeners[id] != null) {
				System.err.println("WARNING: " + id + " has multiple packet handlers!");
			} else {
				clientMessageListeners[id] = listener;
			}
		}
	}

	public void setRunning(boolean running) {
		this.running = running;
		System.out.println("LoginEngine " + (running ? "is" : "has") + " now " + (running ? "running" : "stopped") + ".");
	}

	public boolean isRunning() {
		return running;
	}

}