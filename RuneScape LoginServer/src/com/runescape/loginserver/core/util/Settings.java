package com.runescape.loginserver.core.util;

/**
 * 
 * @author Hikilaka
 *
 */
public final class Settings {
	
	/**
	 * The name of the game
	 */
	public static String NAME = "RuneScape";

	/**
	 * The port number the LoginServer will listen on
	 */
	public static int PORT = 3523;
	
	/**
	 * The number of cores Java Virtual Machine has available
	 */
	public static int CORES = Runtime.getRuntime().availableProcessors();
	
}
