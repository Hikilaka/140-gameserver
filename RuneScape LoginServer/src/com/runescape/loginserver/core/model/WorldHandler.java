package com.runescape.loginserver.core.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class WorldHandler {

	private final Set<World> worlds = new HashSet<>();

	public World getWorld(int id) {
		Iterator<World> it = worlds.iterator();
		while (it.hasNext()) {
			World world = it.next();
			if (world.getId() == id) {
				return world;
			}
		}
		return null;
	}

	public boolean register(World world) {
		if (getWorld(world.getId()) != null) {
			return false;
		}
		worlds.add(world);
		System.out.println("Registered " + (world.isMembers() ? "members " : "") + "world " + world.getId());
		return true;
	}

	public void unregister(World world) {
		if (worlds.remove(world)) {
			System.out.println("Unregistered " + (world.isMembers() ? "members " : "") + "world " + world.getId());
		}
	}

}
