package com.runescape.loginserver.core.model;

public final class World {

	private final int id;

	private final boolean members;

	public World(int id, boolean members) {
		this.id = id;
		this.members = members;
	}

	public int getId() {
		return id;
	}

	public boolean isMembers() {
		return members;
	}
	
	@Override
	public int hashCode() {
		return id;
	}

}
