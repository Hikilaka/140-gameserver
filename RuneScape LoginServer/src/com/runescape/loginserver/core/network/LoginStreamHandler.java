package com.runescape.loginserver.core.network;

import com.runescape.loginserver.core.LoginEngine;
import com.runescape.loginserver.core.LoginServer;
import com.runescape.loginserver.core.model.World;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.Session;
import com.runescape.shared.network.stream.BasicStreamHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class LoginStreamHandler extends BasicStreamHandler {

	private final LoginEngine loginEngine = LoginServer.getLoginServer().getLoginEngine();

	@Override
	protected void onConnected(Session session) {
		System.out.println("Connection established");
	}

	@Override
	protected void onDisconnected(Session session) {
		System.out.println("Connection lost");
		
		if (session.getAttachment() != null) {
			World world = session.getAttachmentAs(World.class);
			System.out.println("World " + world.getId() + " has disconnected!");
		}
	}

	@Override
	protected void onMessage(Session session, Packet packet) {
		System.out.println("Message received");
		loginEngine.queuePacket(packet);
	}

	@Override
	protected void onException(Throwable throwable) {
		System.out.println("WARNING: An exception has occured!");
		throwable.printStackTrace();
	}

}
