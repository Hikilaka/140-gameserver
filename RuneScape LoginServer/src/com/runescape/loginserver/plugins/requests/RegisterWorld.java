package com.runescape.loginserver.plugins.requests;

import com.runescape.loginserver.core.LoginServer;
import com.runescape.loginserver.core.model.World;
import com.runescape.shared.network.ClientMessageListener;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketBuilder;
import com.runescape.shared.network.PacketReadException;

public final class RegisterWorld implements ClientMessageListener {

	private static final int REGISTER_WORLD_ID = 0;

	private static final int UNREGISTER_WORLD_ID = 1;

	@Override
	public int[] getAssociatedIds() {
		return new int[] { REGISTER_WORLD_ID, UNREGISTER_WORLD_ID };
	}

	@Override
	public void onMessage(Packet packet) throws PacketReadException {
		if (packet.getId() == REGISTER_WORLD_ID) {
			registerWorld(packet);
		} else {
			unregisterWorld(packet);
		}
	}
	
	private void registerWorld(Packet packet) throws PacketReadException {
		int worldId = packet.readByte();
		boolean members = packet.readBoolean();
		World world = new World(worldId, members);

		int response = LoginServer.getLoginServer().getWorldHandler().register(world) ? 1 : 0;

		PacketBuilder builder = new PacketBuilder(1).setId(0);
		builder.addByte(response);
		packet.getSession().write(builder);
	}

	private void unregisterWorld(Packet packet) throws PacketReadException {
		
	}
	
}
