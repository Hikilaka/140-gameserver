package com.runescape.gameserver.core;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import com.runescape.gameserver.core.loginserver.LoginConnector;
import com.runescape.gameserver.core.network.GamePipelineFactory;
import com.runescape.gameserver.core.util.Settings;
import com.runescape.gameserver.plugins.PluginHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class GameServer {

	private static final GameServer INSTANCE = new GameServer();

	public static GameServer getGameServer() {
		return INSTANCE;
	}

	/**
	 * The main entry point of the application
	 * 
	 * @param args The arguments passed into the command line
	 */
	public static void main(String[] args) throws Exception {
		Settings.loadSettings();
		System.out.println("Starting up " + Settings.NAME);
		System.out.println(" * Port: " + Settings.PORT);
		System.out.println(" * Version: " + Settings.VERSION);
		System.out.println(" * World: " + Settings.WORLD);
		System.out.println(" * Members: " + Settings.MEMBERS);
		System.out.println();

		System.out.println("Loading plugins.. ");
		PluginHandler.getInstance().loadPlugins();

		System.out.println("Initiating server socket..");
		INSTANCE.initiate();

		System.out.println(Settings.NAME + " is now online!");
	}

	private final GameEngine gameEngine = GameEngine.getGameEngine();

	private final LoginConnector loginConnector = LoginConnector.getLoginConnector();

	private Channel channel;

	/**
	 * Initiates the server and start the networking
	 */
	private void initiate() {
		ExecutorService workers = Executors.newFixedThreadPool(Settings.CORES * 2);
		ChannelFactory channelFactory = new NioServerSocketChannelFactory(workers, workers);
		ServerBootstrap bootstrap = new ServerBootstrap(channelFactory);

		bootstrap.setPipelineFactory(new GamePipelineFactory());
		bootstrap.setOption("tcpNoDelay", true);
		bootstrap.setOption("keepAlive", false);

		channel = bootstrap.bind(new InetSocketAddress(Settings.PORT));

		if (channel.isBound()) {
			gameEngine.setRunning(true);
			gameEngine.start();
		}

		//loginConnector.initiate();
	}

	public void shutdown() {
		System.out.println("Shutting down " + Settings.NAME + "..");
		//blah blah save the players
		//kick them
		gameEngine.setRunning(false);
		ChannelFuture future = channel.close();
		future.addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				System.out.println(Settings.NAME + " has shut down.");
			}
		});
	}

}