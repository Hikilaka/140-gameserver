package com.runescape.gameserver.core.task.impl;

import com.runescape.gameserver.core.loginserver.LoginConnector;
import com.runescape.gameserver.core.task.TaskEvent;

public final class LoginServerReconnectEvent implements TaskEvent {

	private final LoginConnector loginConnector;

	private int ticks = 0;

	public LoginServerReconnectEvent(LoginConnector loginConnector) {
		this.loginConnector = loginConnector;
	}

	@Override
	public boolean satisfied() {
		return ++ticks >= 100;
	}

	@Override
	public boolean remove() {
		return ticks >= 100;
	}

	@Override
	public void handle() {
		loginConnector.initiate();
	}

}
