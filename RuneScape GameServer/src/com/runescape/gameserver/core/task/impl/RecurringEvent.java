package com.runescape.gameserver.core.task.impl;

import com.runescape.gameserver.core.GameEngine;
import com.runescape.gameserver.core.task.TaskEvent;

/**
 * A <code>TaskEvent</code> that will invoke <code>run</code> every time the
 * delay has passed. The task will be removed from the game engine queue once
 * <code>running</code> has been set to <code>false</code>
 * 
 * @author Hikilaka
 * 
 */
public abstract class RecurringEvent implements TaskEvent {

	private final int delay;
	
	private int ticks = 0;

	protected boolean running = true;

	public RecurringEvent(int delay) {
		this.delay = delay / GameEngine.TICK_RATE;
	}

	@Override
	public boolean satisfied() {
		return ++ticks >= delay;
	}

	@Override
	public boolean remove() {
		return !running;
	}

	@Override
	public void handle() {
		run();
		ticks = 0;
	}

	public abstract void run();

}