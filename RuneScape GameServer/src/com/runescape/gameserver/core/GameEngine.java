package com.runescape.gameserver.core;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import com.runescape.gameserver.core.entity.EntityList;
import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.gameserver.core.loginserver.LoginConnector;
import com.runescape.gameserver.core.task.TaskEvent;
import com.runescape.gameserver.core.world.World;
import com.runescape.shared.network.ClientMessageListener;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketReadException;

/**
 * The central hub of the {@link GameServer}. The
 * game engine handles everything from handling player packets
 * to updating players of entities (when needed)
 * 
 * @author Hikilaka
 *
 */
public final class GameEngine extends Thread {

	public static final int TICK_RATE = 150;

	private static final GameEngine INSTANCE = new GameEngine();

	public static GameEngine getGameEngine() {
		return INSTANCE;
	}

	/**
	 * Whether or not the game engine is running
	 */
	private volatile boolean running = false;

	/**
	 * A queue of received packets to be processed the next tick
	 */
	private final LinkedBlockingQueue<Packet> queuedPackets = new LinkedBlockingQueue<>();

	/**
	 * A queue of {@link TaskEvent}s to be processed each game tick
	 */
	private final LinkedBlockingQueue<TaskEvent> queuedTasks = new LinkedBlockingQueue<>();

	/**
	 * An array of ClientMessageListeners to handle incoming packets 
	 */
	private final ClientMessageListener[] clientMessageListeners = new ClientMessageListener[256];

	private final World world = World.getWorld();

	private final LoginConnector loginConnector = LoginConnector.getLoginConnector();

	/**
	 * Starts the game engine, by beginning the game loop,
	 * updating players, npcs, objects, etc.
	 * 
	 * The loop calls <code>tick()</code> every time it's ran,
	 * and update every four ticks
	 * 
	 * In turn, tick runs every 150ms, update 600ms
	 */
	@Override
	public void run() {
		long last = System.currentTimeMillis();
		int ticks = 0;

		while (running) {
			long start = System.currentTimeMillis();
			tick();

			if (++ticks == 4) {
				update();
				ticks = 0;
			}

			long sleeptime = TICK_RATE - (start - last);
			if (sleeptime > 0) {
				try {
					Thread.sleep(sleeptime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				System.err.println("WARNING: " + (100 + (Math.abs(sleeptime) / (TICK_RATE / 100))) + "% server overload!");
			}
			last = System.currentTimeMillis();
		}
	}

	/**
	 * Ticks the game, which includes handling and received packets,
	 * and ticking any events
	 */
	private void tick() {
		tickPacketQueue();
		tickTaskQueue();
		loginConnector.tick();
	}

	/**
	 * Updates the connected players of surrounding entities
	 */
	private void update() {
		EntityList<Player> players = world.getPlayers();
		Iterator<Player> iterator = players.iterator();

		while (iterator.hasNext()) {
			Player player = iterator.next();
			Packet playerPositionPacket = player.getPlayerPositionUpdater().getUpdatePacket();
			player.getSession().write(playerPositionPacket);
		}
	}

	/**
	 * Processes incoming packets
	 */
	private void tickPacketQueue() {
		int processedPackets = 0;
		Iterator<Packet> it = queuedPackets.iterator();

		// process the packets when available, but only up to
		// 150 per tick, which is 1,000 packets per a second
		while (it.hasNext() && processedPackets <= 150) {
			Packet packet = it.next();
			ClientMessageListener handler = clientMessageListeners[packet.getId()];

			if (handler != null) {
				try {
					handler.onMessage(packet);
				} catch (PacketReadException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("WARNING: No packet handler for: " + packet);
			}

			// Remove the packet from the queue
			it.remove();
			// And increment the processed count
			processedPackets++;
		}
	}

	/**
	 * Processes all binded TaskEvents
	 */
	private void tickTaskQueue() {
		Iterator<TaskEvent> it = queuedTasks.iterator();

		while (it.hasNext()) {
			TaskEvent event = it.next();
			if (event.satisfied()) {
				event.handle();
			}
			if (event.remove()) {
				it.remove();
			}
		}
	}

	/**
	 * Adds a packet to the game engine queue, to be 
	 * processed the next tick
	 * 
	 * @param packet The packet to queue
	 */
	public void queuePacket(Packet packet) {
		synchronized (queuedPackets) {
			queuedPackets.add(packet);
		}
	}

	/**
	 * Adds a {@link ClientMessageListener} to an array if not already present
	 * 
	 * @param listener The packet listener to register
	 */
	public void registerClientMessageListener(ClientMessageListener listener) {
		for (int id : listener.getAssociatedIds()) {
			if (clientMessageListeners[id] != null) {
				System.err.println("WARNING: " + id + " has multiple packet handlers (GameEngine)!");
			} else {
				clientMessageListeners[id] = listener;
			}
		}
	}

	/**
	 * Adds a {@link TaskEvent} to a queue, to be processed
	 * each game engine tick
	 * 
	 * @param event The event to be processed
	 */
	public void addTask(TaskEvent event) {
		queuedTasks.add(event);
	}

	public void setRunning(boolean running) {
		this.running = running;
		System.out.println("GameEngine " + (running ? "is" : "has") + " now " + (running ? "running" : "stopped") + ".");
	}

	public boolean isRunning() {
		return running;
	}

}
