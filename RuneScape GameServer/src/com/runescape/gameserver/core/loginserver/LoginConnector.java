package com.runescape.gameserver.core.loginserver;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import com.runescape.gameserver.core.network.loginserver.LoginPipelineFactory;
import com.runescape.gameserver.core.util.Settings;
import com.runescape.shared.network.ClientMessageListener;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketReadException;
import com.runescape.shared.network.Session;

public final class LoginConnector {

	private static final LoginConnector INSTANCE = new LoginConnector();

	public static LoginConnector getLoginConnector() {
		return INSTANCE;
	}

	private final LinkedBlockingQueue<Packet> queuedPackets = new LinkedBlockingQueue<>();

	private final ClientMessageListener[] clientMessageListeners = new ClientMessageListener[256];

	private final LoginServerActionSender actionSender = new LoginServerActionSender(this);

	private final ChannelFactory channelFactory = new NioClientSocketChannelFactory();

	private final LoginPipelineFactory pipelineFactory = new LoginPipelineFactory(this);

	private Channel channel;

	private Session session;

	public void initiate() {
		if (channel == null) {
			System.out.println("Attempting to connect to login server..");
		}
		ClientBootstrap bootstrap = new ClientBootstrap();
		bootstrap.setFactory(channelFactory);
		bootstrap.setPipelineFactory(pipelineFactory);

		bootstrap.setOption("tcpNoDelay", true);
		bootstrap.setOption("keepAlive", false);

		ChannelFuture future = bootstrap.connect(new InetSocketAddress(Settings.LOGIN_SERVER_IP, Settings.LOGIN_SERVER_PORT));
		System.out.println("waiting.. ");

		future.addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				System.out.println("completed operation!");
				if (future.isSuccess()) {
					System.out.println("success");
				}
			}
		});
		
		if (future.isSuccess()) {
			System.out.println("finished");
		}

		/*if (future.isSuccess()) {
			channel = future.getChannel();
			session = new Session(channel);
			session.setAttachment(this);
			actionSender.register();
			System.out.println("Connected to login server!");
		} else {
			System.out.println("Failed to connect to login server, retrying in 15 seconds.. ");
			if (future.getCause() != null) {
				future.getCause().printStackTrace();
			}
			GameEngine.getGameEngine().addTask(new LoginServerReconnectEvent(this));
		}*/
	}

	public void tick() {
		Iterator<Packet> it = queuedPackets.iterator();

		while (it.hasNext()) {
			Packet packet = it.next();
			ClientMessageListener handler = clientMessageListeners[packet.getId()];

			if (handler != null) {
				try {
					handler.onMessage(packet);
				} catch (PacketReadException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("WARNING: No packet handler for: " + packet.getId() + " " + Arrays.toString(packet.getData()));
			}

			it.remove();
		}
	}

	public void queuePacket(Packet packet) {
		synchronized (queuedPackets) {
			queuedPackets.add(packet);
		}
	}

	public void registerClientMessageListener(ClientMessageListener listener) {
		for (int id : listener.getAssociatedIds()) {
			if (clientMessageListeners[id] != null) {
				System.err.println("WARNING: " + id + " has multiple packet handlers (LoginConnector)!");
			} else {
				clientMessageListeners[id] = listener;
			}
		}
	}

	public LoginServerActionSender getActionSender() {
		return actionSender;
	}

	public Session getSession() {
		return session;
	}

	public boolean isConnected() {
		return channel.isConnected();
	}

}