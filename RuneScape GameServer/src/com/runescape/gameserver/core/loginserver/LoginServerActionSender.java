package com.runescape.gameserver.core.loginserver;

import com.runescape.gameserver.core.util.Settings;
import com.runescape.shared.network.PacketBuilder;

/**
 * 
 * @author Hikilaka
 *
 */
public final class LoginServerActionSender {

	private final LoginConnector loginConnector;

	public LoginServerActionSender(LoginConnector loginConnector) {
		this.loginConnector = loginConnector;
	}

	public void register() {
		PacketBuilder builder = new PacketBuilder(1).setId(0);
		builder.addByte(Settings.WORLD);
		loginConnector.getSession().write(builder);
	}

}
