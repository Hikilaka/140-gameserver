package com.runescape.gameserver.core.event;

import java.util.Iterator;

public final class EventChainContext<E extends Event> {

	private final E event;

	private final Iterator<EventListener<E>> iterator;

	private boolean stopped;

	public EventChainContext(E event, Iterator<EventListener<E>> iterator) {
		this.event = event;
		this.iterator = iterator;
	}

	private void checkState() {
		if (isFinished()) {
			throw new IllegalStateException();
		}
	}

	public void breakChain() {
		stopped = true;
	}

	public void doNext() {
		checkState();
		EventListener<E> handler = iterator.next();
		handler.handle(event, this);
	}

	public void doAll() {
		checkState();
		do {
			EventListener<E> handler = iterator.next();
			handler.handle(event, this);
		} while(!isFinished());
	}

	public boolean isFinished() {
		return !iterator.hasNext() || stopped;
	}

}
