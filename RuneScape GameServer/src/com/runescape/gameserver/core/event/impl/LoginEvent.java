package com.runescape.gameserver.core.event.impl;

import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.gameserver.core.event.Event;
import com.runescape.shared.enumerations.LoginStatus;

public final class LoginEvent extends Event {
	
	private final Player player;
	
	private final LoginStatus status;
	
	public LoginEvent(Player player, LoginStatus status) {
		this.player = player;
		this.status = status;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public LoginStatus getStatus() {
		return status;
	}

}
