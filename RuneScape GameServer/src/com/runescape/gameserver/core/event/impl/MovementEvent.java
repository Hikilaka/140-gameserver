package com.runescape.gameserver.core.event.impl;

import com.runescape.gameserver.core.entity.Entity;
import com.runescape.gameserver.core.event.Event;
import com.runescape.shared.util.Location;

public final class MovementEvent extends Event {

	private final Entity entity;
	
	private final Location newLocation, oldLocation;
	
	public MovementEvent(Entity entity, Location newLocation, Location oldLocation) {
		this.entity = entity;
		this.newLocation = newLocation;
		this.oldLocation = oldLocation;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	public Location getNewLocation() {
		return newLocation;
	}
	
	public Location getOldLocation() {
		return oldLocation;
	}
	
}
