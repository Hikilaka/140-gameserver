package com.runescape.gameserver.core.listeners;

import com.runescape.gameserver.core.entity.Entity;
import com.runescape.gameserver.core.entity.LivingEntity;
import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.gameserver.core.event.EventChainContext;
import com.runescape.gameserver.core.event.EventListener;
import com.runescape.gameserver.core.event.impl.MovementEvent;
import com.runescape.shared.util.Location;

public final class MovementEventListener extends EventListener<MovementEvent> {

	private final int[][] sprites = new int[][]{ { 3, 2, 1 }, { 4, -1, 0 }, { 5, 6, 7 } };

	@Override
	public void handle(MovementEvent event, EventChainContext<MovementEvent> ctx) {
		Entity entity = event.getEntity();
		Location newLocation = event.getNewLocation();
		Location oldLocation = event.getOldLocation();

		if (entity instanceof LivingEntity) {
			LivingEntity living = LivingEntity.class.cast(entity);
			int xindex = oldLocation.getX() - newLocation.getX() + 1;
			int yindex = oldLocation.getY() - newLocation.getY() + 1;
			//living.setSprite(sprites[xindex][yindex]);

			if (living instanceof Player) {
				Player player = Player.class.cast(living);

				if (!newLocation.equals(oldLocation)) {
					//TODO update players in view
					player.getPlayerPositionUpdater().setRequiresUpdate(true);
					System.out.println(player.getUsername() + " has moved!");
				}		
			}
		}
	}

}