package com.runescape.gameserver.core.network;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;

import com.runescape.shared.network.stream.DownstreamHandler;
import com.runescape.shared.network.stream.UpstreamHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class GamePipelineFactory implements ChannelPipelineFactory {

	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline = Channels.pipeline();
		pipeline.addLast("down-stream", new DownstreamHandler());
		pipeline.addLast("up-stream", new UpstreamHandler());
		pipeline.addLast("stream-handler", new GameStreamHandler());
		return pipeline;
	}

}
