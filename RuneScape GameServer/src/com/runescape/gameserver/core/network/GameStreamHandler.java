package com.runescape.gameserver.core.network;

import com.runescape.gameserver.core.GameEngine;
import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.gameserver.core.world.World;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketBuilder;
import com.runescape.shared.network.Session;
import com.runescape.shared.network.stream.BasicStreamHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class GameStreamHandler extends BasicStreamHandler {

	private final GameEngine gameEngine = GameEngine.getGameEngine();

	private final World world = World.getWorld();

	@Override
	protected void onConnected(Session session) {
		// The client waits until we send a session id,
		// so let's send one!
		PacketBuilder pb = new PacketBuilder(4).setBare(true);
		pb.addInt(session.hashCode());
		session.write(pb);
	}

	@Override
	protected void onDisconnected(Session session) {
		// Release system resources
		if (session.getAttachment() instanceof Player) {
			world.remove(session.getAttachmentAs(Player.class));
		}
	}

	@Override
	protected void onMessage(Session session, Packet packet) {
		gameEngine.queuePacket(packet);
	}

	@Override
	protected void onException(Throwable throwable) {
		throwable.printStackTrace();
	}

}