package com.runescape.gameserver.core.network.loginserver;

import org.jboss.netty.channel.Channel;

import com.runescape.gameserver.core.loginserver.LoginConnector;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.Session;
import com.runescape.shared.network.stream.BasicStreamHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class LoginStreamHandler extends BasicStreamHandler {

	private final LoginConnector loginConnector;

	public LoginStreamHandler(LoginConnector loginConnector) {
		this.loginConnector = loginConnector;
	}

	@Override
	protected void onConnected(Session session) {
		System.out.println("connected?");
		Channel channel = session.channel();
		if (channel.isConnected()) {
			System.out.println("its connected");
		}
		if (channel.getRemoteAddress() == null) {
			System.out.println("\tnull address");
		} else {
			System.out.println("\t" + session.getIp());
		}
	}

	@Override
	protected void onDisconnected(Session sessions) {
		System.out.println("Lost connection to login server, attempting to re-establish..");
		loginConnector.initiate();
	}

	@Override
	protected void onMessage(Session session, Packet packet) {
		System.out.println("Message received");
		loginConnector.queuePacket(packet);
	}

	@Override
	protected void onException(Throwable throwable) {
		System.out.println("WARNING: An exception has occured!");
		throwable.printStackTrace();
	}

}
