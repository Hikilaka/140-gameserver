package com.runescape.gameserver.core.network.loginserver;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;

import com.runescape.gameserver.core.loginserver.LoginConnector;
import com.runescape.shared.network.stream.DownstreamHandler;
import com.runescape.shared.network.stream.UpstreamHandler;

/**
 * 
 * @author Hikilaka
 *
 */
public final class LoginPipelineFactory implements ChannelPipelineFactory {

	private final LoginConnector loginConnector;

	public LoginPipelineFactory(LoginConnector loginConnector) {
		this.loginConnector = loginConnector;
	}

	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline = Channels.pipeline();
		pipeline.addLast("down-stream", new DownstreamHandler());
		pipeline.addLast("up-stream", new UpstreamHandler());
		pipeline.addLast("stream-handler", new LoginStreamHandler(loginConnector));
		return pipeline;
	}

}
