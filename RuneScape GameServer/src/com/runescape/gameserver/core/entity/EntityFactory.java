package com.runescape.gameserver.core.entity;

import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.gameserver.core.event.EventDispatcher;
import com.runescape.gameserver.core.event.impl.MovementEvent;
import com.runescape.gameserver.core.listeners.MovementEventListener;
import com.runescape.shared.network.Session;
import com.runescape.shared.util.Location;

public final class EntityFactory {

	public static Player newPlayer(Session session, long user, String pass, int referer, int seed) {
		Player player = new Player(session, user, pass, referer, seed);

		EventDispatcher dispatcher = player.getEventDispatcher();
		dispatcher.addListener(MovementEvent.class, new MovementEventListener());

		player.setLocation(new Location(305, 402));
		return player;
	}

}
