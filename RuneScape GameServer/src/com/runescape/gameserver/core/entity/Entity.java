package com.runescape.gameserver.core.entity;

import com.runescape.gameserver.core.event.EventDispatcher;
import com.runescape.gameserver.core.event.impl.MovementEvent;
import com.runescape.shared.util.Location;

public abstract class Entity {

	private final EventDispatcher eventDispatcher = new EventDispatcher();

	private int index = -1;

	private Location location = new Location(300, 300);

	public EventDispatcher getEventDispatcher() {
		return eventDispatcher;
	}

	public void setIndex(int index) {
		if (this.index == -1) {
			this.index = index;
		}
	}

	public int getIndex() {
		return index;
	}

	public void setLocation(Location location) {
		eventDispatcher.fireEvent(new MovementEvent(this, location, this.location));
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

}