package com.runescape.gameserver.core.entity;

/**
 * 
 * @author Hikilaka
 *
 */
public abstract class LivingEntity extends Entity {

	private int sprite = 1;

	public void setSprite(int sprite) {
		this.sprite = sprite;
	}
	
	public int getSprite() {
		return sprite;
	}

	public boolean inCombat() {
		return (sprite == 8 || sprite == 9);
	}

}
