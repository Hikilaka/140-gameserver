package com.runescape.gameserver.core.entity;

import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.shared.network.Packet;

public abstract class EntityUpdater {

	protected final Player player;

	protected boolean requiresUpdate = true;

	protected Packet packet;

	public EntityUpdater(Player player) {
		this.player = player;
	}

	protected abstract Packet constructUpdate();

	public void setRequiresUpdate(boolean requiresUpdate) {
		this.requiresUpdate = requiresUpdate;
	}

	public boolean requiresUpdate() {
		return requiresUpdate;
	}

	public Packet getUpdatePacket() {
		if (requiresUpdate) {
			packet = constructUpdate();
			requiresUpdate = false;
		}
		return packet;
	}

}