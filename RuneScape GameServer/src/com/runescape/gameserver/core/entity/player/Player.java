package com.runescape.gameserver.core.entity.player;

import com.runescape.gameserver.core.entity.LivingEntity;
import com.runescape.gameserver.core.util.Conversions;
import com.runescape.shared.network.Session;

/**
 * 
 * @author Hikilaka
 *
 */
public final class Player extends LivingEntity {

	private final Session session;

	private final String username, password;

	private final long userhash;

	private final int referer, seed;

	private final PlayerPositionUpdater playerPositionUpdater = new PlayerPositionUpdater(this);

	private final ActionSender actionSender = new ActionSender(this);

	public Player(Session session, long user, String pass, int referer, int seed) {
		this.session = session;
		this.username = Conversions.decodeBase37(user);
		this.userhash = user;
		this.password = pass;
		this.referer = referer;
		this.seed = seed;
	}

	public Session getSession() {
		return session;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public long getUserhash() {
		return userhash;
	}

	public int getReferer() {
		return referer;
	}

	public int getSeed() {
		return seed;
	}

	public PlayerPositionUpdater getPlayerPositionUpdater() {
		return playerPositionUpdater;
	}

	public ActionSender getActionSender() {
		return actionSender;
	}

}
