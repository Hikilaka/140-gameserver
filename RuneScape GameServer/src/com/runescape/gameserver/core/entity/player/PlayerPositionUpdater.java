package com.runescape.gameserver.core.entity.player;

import com.runescape.gameserver.core.entity.EntityUpdater;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketBuilder;

public final class PlayerPositionUpdater extends EntityUpdater {

	public PlayerPositionUpdater(Player player) {
		super(player);
	}

	@Override
	protected Packet constructUpdate() {
		PacketBuilder pb = new PacketBuilder().setId(255);
		pb.addBits(player.getLocation().getX(), 10);
		pb.addBits(player.getLocation().getY(), 12);
		pb.addBits(player.getSprite(), 4);
		pb.addBits(0, 8); //no existing players
		return pb.toPacket();
	}

}
