package com.runescape.gameserver.core.entity.player;

import com.runescape.shared.network.PacketBuilder;

/**
 * 
 * @author Hikilaka
 *
 */
public final class ActionSender {

	private final Player player;

	public ActionSender(Player player) {
		this.player = player;
	}

	public void sendWorldInfo() {
		PacketBuilder pb = new PacketBuilder(10).setId(244);
		pb.addShort(player.getIndex());
		pb.addShort(2304);
		pb.addShort(1776);
		pb.addShort(player.getLocation().getHeight());
		pb.addShort(944);
		player.getSession().write(pb);
	}
}
