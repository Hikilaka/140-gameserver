package com.runescape.gameserver.core.util;

import java.math.BigInteger;

import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketReadException;

public final class Conversions {

	public static long encodeBase37(String str) {
		String fmt = "";
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= 'a' && c <= 'z') {
				fmt = fmt + c;
			} else if (c >= 'A' && c <= 'Z') {
				fmt = fmt + (char)((c + 97) - 65);
			} else if (c >= '0' && c <= '9') {
				fmt = fmt + c;
			} else {
				fmt = fmt + ' ';
			}
		}

		fmt = fmt.trim();
		if (fmt.length() > 12) {
			fmt = fmt.substring(0, 12);
		}

		long base = 0L;
		for (int i = 0; i < fmt.length(); i++) {
			char c1 = fmt.charAt(i);
			base *= 37L;
			if (c1 >= 'a' && c1 <= 'z')
				base += (1 + c1) - 97;
			else if(c1 >= '0' && c1 <= '9')
				base += (27 + c1) - 48;
		}
		return base;
	}

	public static String decodeBase37(long base) {
		if (base < 0L) {
			return "invalid_name";
		}
		String str = "";
		while (base != 0L) {
			int value = (int) (base % 37L);
			base /= 37L;
			if (value == 0) {
				str = " " + str;
			} else if (value < 27) {
				if (base % 37L == 0L) {
					str = (char) ((value + 65) - 1) + str;
				} else {
					str = (char) ((value + 97) - 1) + str;
				}
			} else {
				str = (char) ((value + 48) - 27) + str;
			}
		}
		return str;
	}

	public static String decodeBase47(long base) {
		return null;
	}

	public static String decodeRSA(Packet packet, int sessionId) throws PacketReadException {
		String password = "";
		do {
			int length = packet.readByte();
			byte[] magnitude = packet.readBytes(length);
			byte[] decoded = new BigInteger(1, magnitude).modPow(Settings.EXPONENT, Settings.MODULUS).toByteArray();
			Packet p = new Packet(null, -1, decoded).skip(4); //skip the random bytes
			int id = p.readInt();

			if (id != sessionId) {
				//uhm what? this should return a invalid response client side
				return null;
			}

			byte[] chars = p.readBytes(p.remaining());
			password += new String(chars);
		} while (packet.remaining() > 4);
		return password.trim();
	}

}
