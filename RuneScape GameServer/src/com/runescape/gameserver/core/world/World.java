package com.runescape.gameserver.core.world;

import com.runescape.gameserver.core.entity.EntityList;
import com.runescape.gameserver.core.entity.player.Player;

/**
 * 
 * @author Hikilaka
 *
 */
public final class World {
	
	private static final World INSTANCE = new World();
	
	public static World getWorld() {
		return INSTANCE;
	}

	private final EntityList<Player> players = new EntityList<>();

	public boolean register(Player player) {
		System.out.println("Registered player " + player.getUsername());
		return players.add(player);
	}

	public void remove(Player player) {
		System.out.println("Removed player " + player.getUsername());
		players.remove(player);
	}
	
	public EntityList<Player> getPlayers() {
		return players;
	}

}