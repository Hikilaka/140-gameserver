package com.runescape.gameserver.plugins.notifiers;

import com.runescape.shared.network.ClientMessageListener;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketReadException;
import com.runescape.shared.network.util.PacketHandlerType;

/**
 * 
 * @author Hikilaka
 *
 */
public final class Ping implements ClientMessageListener {

	private static final int PING_PACKET = 5;

	@Override
	public PacketHandlerType getHandlerType() {
		return PacketHandlerType.GAME;
	}

	@Override
	public int[] getAssociatedIds() {
		return new int[] { PING_PACKET };
	}

	@Override
	public void onMessage(Packet packet) throws PacketReadException {
		// uhhh what
	}

}
