package com.runescape.gameserver.plugins.requests;

import com.runescape.gameserver.core.GameEngine;
import com.runescape.gameserver.core.entity.EntityFactory;
import com.runescape.gameserver.core.entity.player.Player;
import com.runescape.gameserver.core.event.impl.LoginEvent;
import com.runescape.gameserver.core.task.impl.OneTimeEvent;
import com.runescape.gameserver.core.util.Conversions;
import com.runescape.gameserver.core.util.Settings;
import com.runescape.gameserver.core.world.World;
import com.runescape.shared.enumerations.LoginStatus;
import com.runescape.shared.network.ClientMessageListener;
import com.runescape.shared.network.Packet;
import com.runescape.shared.network.PacketBuilder;
import com.runescape.shared.network.PacketReadException;
import com.runescape.shared.network.Session;
import com.runescape.shared.network.util.PacketHandlerType;

/**
 * A packet handler for login requests
 * 
 * @author Hikilaka
 *
 */
public final class Login implements ClientMessageListener {

	private static final int LOGIN_PACKET = 0;

	private static final int RECONNECT_PACKET = 19;

	private final GameEngine engine = GameEngine.getGameEngine();

	@Override
	public PacketHandlerType getHandlerType() {
		return PacketHandlerType.GAME;
	}
	
	@Override
	public int[] getAssociatedIds() {
		return new int[] { LOGIN_PACKET, RECONNECT_PACKET };
	}

	@Override
	public void onMessage(final Packet packet) throws PacketReadException {
		final Session session = packet.getSession();
		final int sessionId = session.hashCode();
		final int version = packet.readShort();
		final int referer = packet.readShort();
		final long hash = packet.readLong();
		final String password = Conversions.decodeRSA(packet, sessionId);
		final int seed = packet.readInt();

		System.out.println("version = " + version);
		
		engine.addTask(new OneTimeEvent() {
			@Override
			public void run() {
				LoginStatus status = packet.getId() == LOGIN_PACKET ? LoginStatus.ACCEPTED : LoginStatus.RECONNECTED;

				if (version != Settings.VERSION) {
					status = LoginStatus.CLIENT_UPDATED;
				}

				PacketBuilder pb = new PacketBuilder(2).setBare(true);
				pb.addByte(0);
				pb.addByte(status.getCode());
				session.write(pb);
				//TODO: request login from login server	

				Player player = EntityFactory.newPlayer(session, hash, password, referer, seed);
				session.setAttachment(player);
				World.getWorld().register(player);
				player.getEventDispatcher().fireEvent(new LoginEvent(player, LoginStatus.ACCEPTED));
			}
		});
	}

}
