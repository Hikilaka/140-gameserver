package com.runescape.gameserver.plugins;

import java.io.File;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.runescape.gameserver.core.GameEngine;
import com.runescape.gameserver.core.loginserver.LoginConnector;
import com.runescape.shared.network.ClientMessageListener;

/**
 * 
 * @author Hikilaka
 *
 */
public final class PluginHandler {

	private static final PluginHandler INSTANCE = new PluginHandler();

	public static PluginHandler getInstance() {
		return INSTANCE;
	}

	public void loadPlugins() {
		loadPacketHandlers();
	}

	private void loadPacketHandlers() {
		try {
			ArrayList<Class<?>> requests = loadClasses("com.runescape.gameserver.plugins.requests");
			ArrayList<Class<?>> notifiers = loadClasses("com.runescape.gameserver.plugins.notifiers");

			registerClientMessageListeners(requests);
			registerClientMessageListeners(notifiers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerClientMessageListeners(List<Class<?>> listeners) throws Exception {
		for (Class<?> c : listeners) {
			Object instance = c.newInstance();

			if (instance instanceof ClientMessageListener) {
				ClientMessageListener listener = ClientMessageListener.class.cast(instance);
				switch (listener.getHandlerType()) {
				case GAME:
					GameEngine.getGameEngine().registerClientMessageListener(listener);
					break;
				case LOGIN:
					LoginConnector.getLoginConnector().registerClientMessageListener(listener);
					break;
				case BOTH:
					GameEngine.getGameEngine().registerClientMessageListener(listener);
					LoginConnector.getLoginConnector().registerClientMessageListener(listener);
					break;
				}
			}
		}
	}

	private ArrayList<Class<?>> loadClasses(String pckgname) throws ClassNotFoundException {
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		ArrayList<File> directories = new ArrayList<File>();
		try {
			ClassLoader classLoader = PluginHandler.class.getClassLoader();
			Enumeration<URL> resources = classLoader.getResources(pckgname.replace('.', '/'));

			while (resources.hasMoreElements()) {
				URL res = resources.nextElement();
				if (res.getProtocol().equalsIgnoreCase("jar")) {
					JarURLConnection conn = (JarURLConnection) res.openConnection();
					JarFile jar = conn.getJarFile();
					for (JarEntry e : Collections.list(jar.entries())) {
						if (e.getName().startsWith(pckgname.replace('.', '/')) && e.getName().endsWith(".class") && !e.getName().contains("$")) {
							String className = e.getName().replace("/", ".").substring(0, e.getName().length() - 6);
							classes.add(Class.forName(className));
						}
					}
				} else {
					directories.add(new File(URLDecoder.decode(res.getPath(), "UTF-8")));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (File directory : directories) {
			if (directory.exists()) {
				String[] files = directory.list();
				for (String file : files) {
					if (file.endsWith(".class") && !file.contains("$")) {
						classes.add(Class.forName(pckgname + '.' + file.substring(0, file.length() - 6)));
					}
				}
			} else {
				throw new ClassNotFoundException(pckgname + " (" + directory.getPath() + ") does not appear to be a valid package");
			}
		}
		return classes;
	}

}