package com.runescape.shared.enumerations;

public enum LoginStatus {

	ACCEPTED(0), RECONNECTED(1), INVALID_CREDENTIALS(3),
	ALREADY_LOGGED_IN(4), CLIENT_UPDATED(5), IP_ALREADY_IN_USE(6),
	ATTEMPTS_EXCEEDED(7), TEMP_BANNED(11), PERM_BANNED(12),
	USER_ALREADY_TAKEN(13), SERVER_FULL(14), MEMBERS_REQUIRED(15),
	MEMBERS_ACCOUNT(16);

	private final int code;

	LoginStatus(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}