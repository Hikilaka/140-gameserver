package com.runescape.shared.network;

import java.util.Arrays;

/**
 * A packet object that the server will send or receive
 * 
 * @author Hikilaka
 *
 */
public final class Packet {

	private final Session session;

	private final int id;

	private final byte[] data;

	private final int length;

	private final boolean bare;

	private int caret;

	public Packet(Session session, int id, byte[] data) {
		this(session, id, data, false);
	}

	public Packet(Session session, int id, byte[] data, boolean bare) {
		this.session = session;
		this.id = id;
		this.data = data;
		this.length = data.length;
		this.bare = bare;
	}

	public Session getSession() {
		return session;
	}

	public int getId() {
		return id;
	}

	public byte[] getData() {
		return data;
	}

	public int getLength() {
		return length;
	}

	public boolean isBare() {
		return bare;
	}

	public byte[] getRemainingData() {
		byte[] data = new byte[length - caret];
		for (int i = 0; i < data.length; i++) {
			data[i] = data[i + caret];
		}
		caret += data.length;
		return data;
	}

	private void canRead(int amount) throws PacketReadException {
		if (caret + amount > length) {
			throw new PacketReadException("Failed to read " + amount + " bytes in " + this);
		}
	}

	public int readByte() throws PacketReadException {
		canRead(1);
		return data[caret++] & 0xff;
	}

	public boolean readBoolean() throws PacketReadException {
		canRead(1);
		return data[caret++] == 1;
	}

	public short readShort() throws PacketReadException {
		canRead(2);
		short value = 0;
		for (int i = 1; i >= 0; i--) {
			value |= (data[caret++] & 0xff) << (i * 8);
		}
		return value;
	}

	public int readInt() throws PacketReadException {
		canRead(4);
		int value = 0;
		for (int i = 3; i >= 0; i--) {
			value |= (data[caret++] & 0xff) << (i * 8);
		}
		return value;
	}

	public long readLong() throws PacketReadException {
		canRead(8);
		long value = 0;
		for (int i = 7; i >= 0; i--) {
			value |= (data[caret++] & 0xff) << (i * 8);
		}
		return value;
	}

	public byte[] readBytes(int length) throws PacketReadException {
		canRead(length);
		byte[] data = new byte[length];
		for (int i = 0; i < length; i++) {
			data[i] = this.data[i + caret];
		}
		caret += length;
		return data;
	}

	public String readString() throws PacketReadException {
		return readString(length - caret);
	}

	public String readString(int length) throws PacketReadException {
		String rv = new String(data, caret, length);
		caret += length;
		return rv;
	}

	public int remaining() {
		return data.length - caret;
	}

	public Packet skip(int x) {
		caret += x;
		return this;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("Packet(");
		builder.append("id = ").append(id);
		builder.append(", caret = ").append(caret);
		builder.append(", data = ").append(Arrays.toString(data)).append(")");
		return builder.toString();
	}

}