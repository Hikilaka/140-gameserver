package com.runescape.shared.network;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;

/**
 * This class represents a session within the network.
 * It also acts as a wrapper for the {@link Channel} class
 * 
 * @author Hikilaka
 * 
 */
public final class Session {

	private final Channel channel;

	private Object attachment;

	/**
	 * @param key The {@link Channel} to 'wrap'
	 */
	public Session(Channel channel) {
		this.channel = channel;
		channel.setAttachment(this);
	}

	/**
	 * Writes an object to the channel
	 * 
	 * @param object The object to write
	 */
	public ChannelFuture write(Packet packet) {
		if (channel.isWritable()) {
			return channel.write(packet);
		}
		return null;
	}

	public ChannelFuture write(PacketBuilder builder) {
		return write(builder.toPacket());
	}

	/**
	 * Closes this session
	 */
	public ChannelFuture close() {
		return channel.close();
	}

	/**
	 * Sets the <code>attachment<code>
	 * 
	 * @param attachment
	 *            The new attachment
	 */
	public void setAttachment(Object attachment) {
		this.attachment = attachment;
	}

	/**
	 * Gets the attachment
	 * 
	 * @return The attachment as a <code>Object</code>
	 */
	public Object getAttachment() {
		return attachment;
	}

	/**
	 * Gets the attachment as the specified class type
	 * 
	 * @param type
	 *            The <code>Class<code> type to interpret the object as
	 * @return
	 */
	public <T> T getAttachmentAs(Class<T> type) {
		return type.cast(attachment);
	}
	
	public String getIp() {
		String address = channel.getRemoteAddress().toString();
		return address.substring(1, address.indexOf(":"));
	}

	@Override
	public String toString() {
		return "Session(channel=" + channel + ", attachment=" + attachment + ")";
	}

	/**
	 * Returns this instance's hash, it will also serve as the
	 * session's id
	 */
	@Override
	public int hashCode() {
		return channel.getId();
	}
	
	public Channel channel() { return channel; }

}