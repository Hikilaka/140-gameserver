package com.runescape.shared.network;

/**
 * 
 * @author Hikilaka
 *
 */
@SuppressWarnings("serial")
public final class PacketReadException extends Exception {

	public PacketReadException(String text) {
		super(text);
	}
	
}
