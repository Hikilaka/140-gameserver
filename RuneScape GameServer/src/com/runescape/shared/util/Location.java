package com.runescape.shared.util;

public final class Location {

	private final int x, y;

	public Location(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getHeight() {
		return y / 944;
	}

	public String toString() {
		return "Location(" + x + ", " + y + ")";
	}

	@Override
	public int hashCode() {
		return (x << 16) | y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Location)) {
			return false;
		}
		Location other = Location.class.cast(o);
		return x == other.x && y == other.y;
	}

}
